'use strict';

var CityWeekly = angular.module('CityWeekly', [
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'CityWeekly.billing',
    'CityWeekly.store',
    'CityWeekly.login',
    'CityWeekly.review',
    'CityWeekly.confirm'
]);

CityWeekly.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider.otherwise({
        redirectTo: '/',
        templateUrl: 'store/store.html',
        controller: 'storeController'
    });
}]);

CityWeekly.directive('resize', function ($window) {
    return function (scope, element, attr) {
        var w = angular.element($window);
        scope.$watch(function () {
            return {
                'h': window.innerHeight,
                'w': window.innerWidth
            };
        }, function (newValue, oldValue) {
            if (newValue.w >1024) {
                scope.sidebarStatus = null;
            } else {
                scope.sidebarStatus = 'closed';
            }
            console.log(scope.status);
        }, true);
        scope.toggleSidebar = function() {
            if (scope.sidebarStatus == null) {
                scope.sidebarStatus = 'closed';
            } else {
                scope.sidebarStatus = null;
            }
        };
        w.bind('resize', function () {
            scope.$apply();
        });
    }
});

CityWeekly.directive('templates', function () {
    return {
        scope: {
            footer: 'templates/footer.html',
            header: 'templates/header.html',
            modal: 'templates/modal.html',
            sidebar: 'templates/sidebar.html',
            products: 'templates/store.html'
        }
    }
});

CityWeekly.directive('cwHeader', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/header.html'
    }
});

CityWeekly.directive('cwModal', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/modal.html'
    }
});

CityWeekly.directive('cwFooter', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/footer.html'
    }
});

CityWeekly.directive('cwSidebar', function() {
    return {
        restrict: 'E',
        templateUrl: 'templates/sidebar.html',
        link: function(scope, element, attrs, controllers) {
            scope.oneAtATime = true;
            scope.navs = [
                {
                    title: 'OFFERS',
                    id: 'offers',
                    subItems: [
                        {
                            title: 'Pages',
                            id: 'best-utah',
                            subItems: [
                                {title: 'Billing', link: "/billing"},
                                {title: 'Login', link: '/login'},
                                {title: 'Review', link: '/review'},
                                {title: 'Store', link: '/store'}
                            ]
                        },
                        {
                            title: 'New Arrivals',
                            id: 'new-arrivals',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Hotel & Travel',
                            id: 'travel',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Dining & Drinks',
                            id: 'dining-drinks',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        }
                    ],
                },
                {
                    title: 'TICKETS',
                    id: 'tickets',
                    subItems: [
                        {
                            title: 'Featured Events',
                            id: 'featured-events',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Venues',
                            id: 'venues',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Concerts',
                            id: 'concerts',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Upcoming',
                            id: 'upcoming',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        }
                    ]
                }
            ];
            scope.status = {
             isFirstOpen: true,
             isFirstDisabled: false
            };
        }
    }
});

CityWeekly.factory('Store', function() {
    var Store = {};
    Store.products = [
        {
            title: 'Product 1',
            featured: true,
            imgURL: 'assets/img/product1.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 2',
            featured: true,
            imgURL: 'assets/img/product2.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 3',
            featured: true,
            imgURL: 'assets/img/product3.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 4',
            featured: false,
            imgURL: 'assets/img/product4.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 5',
            featured: false,
            imgURL: 'assets/img/product5.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 6',
            featured: false,
            imgURL: 'assets/img/product6.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 7',
            featured: false,
            imgURL: 'assets/img/product7.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 8',
            featured: false,
            imgURL: 'assets/img/product8.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 1',
            featured: false,
            imgURL: 'assets/img/product1.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 2',
            featured: false,
            imgURL: 'assets/img/product2.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 3',
            featured: false,
            imgURL: 'assets/img/product3.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 4',
            featured: false,
            imgURL: 'assets/img/product4.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 5',
            featured: false,
            imgURL: 'assets/img/product5.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 6',
            featured: false,
            imgURL: 'assets/img/product6.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 7',
            featured: false,
            imgURL: 'assets/img/product7.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 8',
            featured: false,
            imgURL: 'assets/img/product8.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        }
    ];
    return Store;
});

CityWeekly.factory('cart', function() {
    var cart = {};
    cart.items = 0;
    return cart;
});

