'use strict';

angular.module('CityWeekly.billing', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/billing', {
            templateUrl: 'billing/billing.html',
            controller: 'billingController'
        });
    }])

    .controller('billingController', [function() {

    }]);