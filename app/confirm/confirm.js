'use strict';

angular.module('CityWeekly.confirm', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/confirm', {
            templateUrl: 'confirm/confirm.html',
            controller: 'confirmationController'
        });
    }])

    .controller('confirmationController', [function() {

    }]);