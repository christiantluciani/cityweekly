'use strict';

angular.module('CityWeekly.review', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/review', {
            templateUrl: 'review/review.html',
            controller: 'reviewController'
        });
    }])

    .controller('reviewController', [function() {

    }]);