'use strict';

angular.module('CityWeekly.store', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/store', {
            templateUrl: 'store/store.html',
            controller: 'storeController'
        });
    }])
    .controller('storeController', ['$scope', 'Store', 'cart', function($scope, Store, cart) {
        $scope.products = {};
        $scope.products = Store.products;
        $scope.totalItems = $scope.products.length;
        $scope.currentPage = 1;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            $log.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.options = [5, 10, 25, 50];
        $scope.maxSize = 3;
        $scope.numPerPage = $scope.options[1];
        $scope.bigTotalItems = 175;
        $scope.bigCurrentPage = 1;
        $scope.$watch('currentPage + numPerPage', function() {
            var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;

            $scope.filteredProducts = $scope.products.slice(begin, end);
        }
    );
}]);