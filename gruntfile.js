module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            options: {
                mangle: false,
                wrap: true
            },
            vendor: {
                files: [{
                    expand: true,
                    src: '*.js',
                    dest: 'app/js/',
                    cwd: 'vendor/',
                    ext: '.min.js'
                }]
            }
        },
        concat: {
            styles: {
                src: [
                    'app/assets/css/bootstrap.min.css',
                    'app/assets/css/bootstrap-theme.min.css',
                    'app/assets/css/open-iconic-bootstrap.min.css',
                    'app/assets/css/open-iconic.css',
                    'app/assets/css/main.css',
                    'app/assets/css/modalStyling.css',
                    'app/assets/css/checkout/billingStyles.css',
                    'app/assets/css/checkout/checkoutStyles.css'
                ],
                dest: 'app/css/main.css'
            },
            scripts: {
                src: [
                    'scripts/main.js',
                    'scripts/directives.js',
                    'scripts/factories.js',
                    'scripts/scripts.js'
                ],
                dest: 'app/app.js'
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['uglify', 'concat']);
};