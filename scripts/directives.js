CityWeekly.directive('resize', function ($window) {
    return function (scope, element, attr) {
        var w = angular.element($window);
        scope.$watch(function () {
            return {
                'h': window.innerHeight,
                'w': window.innerWidth
            };
        }, function (newValue, oldValue) {
            if (newValue.w >1024) {
                scope.sidebarStatus = null;
            } else {
                scope.sidebarStatus = 'closed';
            }
            console.log(scope.status);
        }, true);
        scope.toggleSidebar = function() {
            if (scope.sidebarStatus == null) {
                scope.sidebarStatus = 'closed';
            } else {
                scope.sidebarStatus = null;
            }
        };
        w.bind('resize', function () {
            scope.$apply();
        });
    }
});

CityWeekly.directive('templates', function () {
    return {
        scope: {
            footer: 'templates/footer.html',
            header: 'templates/header.html',
            modal: 'templates/modal.html',
            sidebar: 'templates/sidebar.html',
            products: 'templates/store.html'
        }
    }
});

CityWeekly.directive('cwHeader', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/header.html'
    }
});

CityWeekly.directive('cwModal', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/modal.html'
    }
});

CityWeekly.directive('cwFooter', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/footer.html'
    }
});

CityWeekly.directive('cwSidebar', function() {
    return {
        restrict: 'E',
        templateUrl: 'templates/sidebar.html',
        link: function(scope, element, attrs, controllers) {
            scope.oneAtATime = true;
            scope.navs = [
                {
                    title: 'OFFERS',
                    id: 'offers',
                    subItems: [
                        {
                            title: 'Pages',
                            id: 'best-utah',
                            subItems: [
                                {title: 'Billing', link: "/billing"},
                                {title: 'Login', link: '/login'},
                                {title: 'Review', link: '/review'},
                                {title: 'Store', link: '/store'}
                            ]
                        },
                        {
                            title: 'New Arrivals',
                            id: 'new-arrivals',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Hotel & Travel',
                            id: 'travel',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Dining & Drinks',
                            id: 'dining-drinks',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        }
                    ],
                },
                {
                    title: 'TICKETS',
                    id: 'tickets',
                    subItems: [
                        {
                            title: 'Featured Events',
                            id: 'featured-events',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Venues',
                            id: 'venues',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Concerts',
                            id: 'concerts',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        },
                        {
                            title: 'Upcoming',
                            id: 'upcoming',
                            subItems: [
                                {title: 'second level link'},
                                {title: 'second level link'},
                                {title: 'second level link'}
                            ]
                        }
                    ]
                }
            ];
            scope.status = {
             isFirstOpen: true,
             isFirstDisabled: false
            };
        }
    }
});
