CityWeekly.factory('Store', function() {
    var Store = {};
    Store.products = [
        {
            title: 'Product 1',
            featured: true,
            imgURL: 'assets/img/product1.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 2',
            featured: true,
            imgURL: 'assets/img/product2.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 3',
            featured: true,
            imgURL: 'assets/img/product3.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 4',
            featured: false,
            imgURL: 'assets/img/product4.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 5',
            featured: false,
            imgURL: 'assets/img/product5.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 6',
            featured: false,
            imgURL: 'assets/img/product6.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 7',
            featured: false,
            imgURL: 'assets/img/product7.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 8',
            featured: false,
            imgURL: 'assets/img/product8.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 1',
            featured: false,
            imgURL: 'assets/img/product1.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 2',
            featured: false,
            imgURL: 'assets/img/product2.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 3',
            featured: false,
            imgURL: 'assets/img/product3.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 4',
            featured: false,
            imgURL: 'assets/img/product4.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 5',
            featured: false,
            imgURL: 'assets/img/product5.png',
            stamp: 'tag',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '65',
            discount: '60'
        },
        {
            title: 'Product 6',
            featured: false,
            imgURL: 'assets/img/product6.png',
            stamp: 'credit-card',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 7',
            featured: false,
            imgURL: 'assets/img/product7.png',
            stamp: 'badge',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        },
        {
            title: 'Product 8',
            featured: false,
            imgURL: 'assets/img/product8.png',
            stamp: 'print',
            description: 'Copper Bowl is a traditional, innovative Indian cuisine balance by an array of exotic spices. Copper Bowl offers a perfect blend of northern...',
            price: '150',
            discount: '100'
        }
    ];
    return Store;
});

CityWeekly.factory('cart', function() {
    var cart = {};
    cart.items = 0;
    return cart;
});