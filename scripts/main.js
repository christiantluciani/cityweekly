'use strict';

var CityWeekly = angular.module('CityWeekly', [
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'CityWeekly.billing',
    'CityWeekly.store',
    'CityWeekly.login',
    'CityWeekly.review',
    'CityWeekly.confirm'
]);

CityWeekly.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider.otherwise({
        redirectTo: '/',
        templateUrl: 'store/store.html',
        controller: 'storeController'
    });
}]);
